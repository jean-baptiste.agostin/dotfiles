# 🚀 macOS Development Setup

This guide helps you configure a comprehensive development environment on macOS, incorporating a variety of tools for full-stack developers.

## 📦 What's Included

- **[Homebrew](https://brew.sh)** for package management 🍺
- **[Neovim](https://neovim.io/)** as a text editor ✏️
- **[Fish Shell](https://fishshell.com/)** with **Oh My Fish** for an enhanced terminal experience 🐟
- **[Kitty](https://sw.kovidgoyal.net/kitty/)** as a terminal emulator 🐱
- **[tmux](https://github.com/tmux/tmux)** for terminal multiplexing 🖥
- **[Git](https://git-scm.com/)** for version control 🌿
- **[Ngrok](https://ngrok.com/)** to expose local servers to the web 🌐
- **[Yabai](https://github.com/koekeishiya/yabai)** for macOS window management 🪟
- Various development tools like **Go**, **PHP**, **Node.js**, and **Docker** 🛠

## 🗂 File Structure

Here's the organization of the configuration files (`dotfiles`) and the installation script:

```
.
├── Brewfile
├── config
│   ├── bash
│   │   └── .bashrc
│   ├── fish
│   │   └── config.fish
│   ├── kitty
│   │   └── kitty.conf
│   ├── nvim
│   │   └── init.vim
│   ├── git
│   │   └── .gitconfig
│   ├── tmux
│   │   └── .tmux.conf
│   ├── ngrok
│   │   └── ngrok.yml
│   └── yabai
│       └── .yabairc
└── install.sh
```

## 🛠 Installation

1. Clone this repository to your local directory.
2. Make the `install.sh` script executable with the command: `chmod +x install.sh`.
3. Execute the script with `./install.sh` to install all packages and apply the configurations.

## 📄 Configuration Files

### Bash Shell

The `.bashrc` file customizes the Bash shell, setting up an environment that supports a range of tools and utilities.

### Fish Shell

The `config.fish` file sets up the Fish Shell and installs **Oh My Fish** with the **bobthefish** theme.

### Kitty

The `kitty.conf` file customizes the appearance and behavior of your Kitty terminal.

### Neovim

The `init.vim` file provides a basic configuration for Neovim, including line numbering and syntax highlighting.

### Git

The `.gitconfig` file configures your username, email, and a few handy aliases for Git.

### Tmux

The `.tmux.conf` file enhances the tmux interface and configures system clipboard sharing.

### Ngrok

The `ngrok.yml` file contains your authentication token and basic configuration for starting tunnels.

### Yabai

The `.yabairc` file is used to customize the behavior of Yabai, the window manager for macOS.
