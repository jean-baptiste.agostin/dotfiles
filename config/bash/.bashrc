PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

alias ll='ls -la'
alias ..='cd ..'

export PATH="/usr/local/bin:$PATH"

echo "Environment bash chargé."
