#!/bin/bash

if ! command -v brewé &> /dev/null
then
    echo "Installation de Homebrew..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "Mise à jour de Homebrew..."
    brew update
fi

if [ ! -d "$HOME/.oh-my-zsh" ]; then
    echo "Installing Oh My Zsh..."
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

echo "Installation des paquets via Brewfile..."
brew bundle --file=./Brewfile

echo "Configuration des outils et applications..."
cp -R ./config/fish/config.fish $HOME/.config/fish/
cp -R ./config/kitty/kitty.conf $HOME/.config/kitty/
cp -R ./config/nvim/init.vim $HOME/.config/nvim/
cp ./config/git/.gitconfig $HOME/
cp ./config/tmux/.tmux.conf $HOME/
cp ./config/ngrok/ngrok.yml $HOME/.ngrok2/ngrok.yml
cp ./config/yabai/.yabairc $HOME/.yabairc

cp ./config/bash/.bashrc $HOME/
cp ./config/zsh/.zshrc $HOME/

brew services start yabai
brew services start postgresql
brew services start mysql
brew services start redis

echo "Installation et configuration terminées !"
